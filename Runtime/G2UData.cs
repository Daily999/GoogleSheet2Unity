﻿using System;
using UnityEngine;

namespace Daily.G2U
{
    [Serializable]
    public class G2UData
    {
#if  UNITY_EDITOR
        [SerializeField] private string sheetFileID;
        [SerializeField] private string worksheetName;
#endif
    }
}