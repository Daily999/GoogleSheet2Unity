﻿using System.Collections.Generic;
using UnityEngine;
namespace Daily.G2U
{
    public class G2UDataList<T> : ScriptableObject, IG2UTable<T>
    {
#if UNITY_EDITOR
        [SerializeField] private G2UData g2U;
#endif
        [SerializeField] protected List<T> data;

        public virtual List<T> DataList => data;
        public virtual void UpdateTable(List<T> list)
        {
            data = new List<T>();
            foreach (var weapon in list)
            {
                data.Add(weapon);
            }
        }
    }
}