using UnityEditor;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof($WorkSheetClassName))]
public class  $ClassName : BaseEditor2<$WorkSheetClassName>
{
    public override void Load()
    {
        var targetData = target as $WorkSheetClassName;
        Daily.G2U.DailyG2UManager.GetData(targetData.SheetName, targetData.WorksheetName, (data) =>
        {
            var myDataList = Daily.G2U.FixQuickSheet.SetDatas<$DataClassName>(data);
            targetData.dataArray = myDataList.ToArray();

            EditorUtility.SetDirty(targetData);
            AssetDatabase.SaveAssets();
        });
    }
}
