﻿using UnityEngine;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using Object = System.Object;

namespace Daily.G2U
{
    public class GoogleSheetsHelper
    {
        private readonly SheetsService _sheetsService;
        private readonly string _spreadsheetId;
        
        public GoogleSheetsHelper(string accessToken, string spreadsheetId)
        {
            var credential = GoogleCredential.FromAccessToken(accessToken);
            _sheetsService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });
            _spreadsheetId = spreadsheetId;
        }
        
        private int GetSheetId(SheetsService service, string spreadSheetName)
        {
            var spreadsheet = service.Spreadsheets.Get(_spreadsheetId).Execute();
            var sheet = spreadsheet.Sheets.FirstOrDefault(s => s.Properties.Title == spreadSheetName);
            return (int)sheet.Properties.SheetId;
        }
        
        public void AddWorkSheet(string spreadsheetId,string sheetName ,string title)
        {
            var r= CreateNewWorkSheet(sheetName, out int guid);
            var r2 = PasteData(title, guid);
            var r3 = ChangeBackgroundColor(new UnityEngine.Color(0.42f,0.76f,0.72f,.1f), guid);
//            var r4 = AddRemark("", guid);
            
            var requests = new List<Request>();
            requests.Add(r);
            requests.Add(r2);
            requests.Add(r3);
//            requests.Add(r4);

            var response = BatchUpdate(requests,spreadsheetId).Execute();
            Debug.Log(JsonUtility.ToJson(response));
        }

        private Request AddRemark(string text, int sheetId)
        {
            var r = new Request();
            r.UpdateCells = new UpdateCellsRequest();
            r.UpdateCells.Fields = "note";
            r.UpdateCells.Rows = new List<RowData>();
            r.UpdateCells.Rows.Add(new RowData());
            r.UpdateCells.Rows[0].Values = new List<CellData>();
            r.UpdateCells.Rows[0].Values.Add(new CellData());
            r.UpdateCells.Rows[0].Values[0].Note = text;
            
            r.RepeatCell = new RepeatCellRequest();
            r.RepeatCell.Range = new GridRange();
            r.RepeatCell.Range.SheetId = sheetId;
            r.RepeatCell.Range.StartColumnIndex = 0;
            r.RepeatCell.Range.StartRowIndex = 0;
            r.RepeatCell.Range.EndColumnIndex = 1;
            r.RepeatCell.Range.EndRowIndex = 1;
            return r;
        }

        private Request ChangeBackgroundColor(UnityEngine.Color color, int sheetId)
        {
            var r = new Request();
            r.RepeatCell = new RepeatCellRequest();
            r.RepeatCell.Fields = "UserEnteredFormat(BackgroundColor,TextFormat)";
            
            r.RepeatCell.Cell = new CellData();
            r.RepeatCell.Cell.UserEnteredFormat = new CellFormat();
            r.RepeatCell.Cell.UserEnteredFormat.BackgroundColor = new Google.Apis.Sheets.v4.Data.Color();
            r.RepeatCell.Cell.UserEnteredFormat.BackgroundColor.Red = color.r;
            r.RepeatCell.Cell.UserEnteredFormat.BackgroundColor.Green = color.g;
            r.RepeatCell.Cell.UserEnteredFormat.BackgroundColor.Blue = color.b;
            r.RepeatCell.Cell.UserEnteredFormat.BackgroundColor.Alpha = color.a;
            
            r.RepeatCell.Range = new GridRange();
            r.RepeatCell.Range.SheetId = sheetId;
            r.RepeatCell.Range.StartColumnIndex = 0;
            r.RepeatCell.Range.StartRowIndex = 0;
            r.RepeatCell.Range.EndColumnIndex = 28;
            r.RepeatCell.Range.EndRowIndex = 1;
            return r;
        }
      
        private Request CreateNewWorkSheet(string workSheetName,out int newSheetId)
        {               
            newSheetId = Mathf.Abs(Guid.NewGuid().GetHashCode());
            var r = new Request();
            r.AddSheet = new AddSheetRequest();
            r.AddSheet.Properties = new SheetProperties();
            r.AddSheet.Properties.Title = workSheetName;
            r.AddSheet.Properties.SheetId = newSheetId;
            return r;
        }
        
        private Request PasteData(string text, int sheetId)
        {
            var r = new Request();
            r.PasteData = new PasteDataRequest();
            r.PasteData.Data = text;
            r.PasteData.Delimiter = "\t";
            r.PasteData.Coordinate = new GridCoordinate();
            r.PasteData.Coordinate.RowIndex = 0;
            r.PasteData.Coordinate.ColumnIndex = 0;
            r.PasteData.Coordinate.SheetId = sheetId;
            
            return r;
        }

        private SpreadsheetsResource.BatchUpdateRequest BatchUpdate(List<Request> requests, string spreadsheetId)
        {  
            var requestBody = new BatchUpdateSpreadsheetRequest();
            requestBody.Requests = requests;
            return  _sheetsService.Spreadsheets.BatchUpdate(requestBody, spreadsheetId);
        }
        /*
               
        public List<ExpandoObject> GetDataFromSheet(GoogleSheetParameters googleSheetParameters)
        {
            googleSheetParameters = MakeGoogleSheetDataRangeColumnsZeroBased(googleSheetParameters);
            var range = $"{googleSheetParameters.SheetName}!{GetColumnName(googleSheetParameters.RangeColumnStart)}{googleSheetParameters.RangeRowStart}:{GetColumnName(googleSheetParameters.RangeColumnEnd)}{googleSheetParameters.RangeRowEnd}";

            SpreadsheetsResource.ValuesResource.GetRequest request =
                _sheetsService.Spreadsheets.Values.Get(_spreadsheetId, range);

            var numberOfColumns = googleSheetParameters.RangeColumnEnd - googleSheetParameters.RangeColumnStart;
            var columnNames = new List<string>();
            var returnValues = new List<ExpandoObject>();

            if (!googleSheetParameters.FirstRowIsHeaders)
            {
                for (var i = 0;i<=numberOfColumns;i++)
                {
                    columnNames.Add($"Column{i}");
                }
            }

            var response = request.Execute();

            int rowCounter = 0;
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                foreach (var row in values)
                {
                    if (googleSheetParameters.FirstRowIsHeaders && rowCounter == 0)
                    {
                        for (var i = 0; i <= numberOfColumns; i++)
                        {
                            columnNames.Add(row[i].ToString());
                        }
                        rowCounter++;
                        continue;
                    }

                    var expando = new ExpandoObject();
                    var expandoDict = expando as IDictionary<String, object>;
                    var columnCounter = 0;
                    foreach (var columnName in columnNames)
                    {
                        expandoDict.Add(columnName, row[columnCounter].ToString());
                        columnCounter++;
                    }
                    returnValues.Add(expando);
                    rowCounter++;
                }
            }

            return returnValues;
        }
 
        public void AddCells(GoogleSheetParameters googleSheetParameters, List<GoogleSheetRow> rows)
        {
            var requests = new BatchUpdateSpreadsheetRequest {Requests = new List<Request>()};
            var sheetId = GetSheetId(_sheetsService, googleSheetParameters.SheetName);

            GridCoordinate gc = new GridCoordinate
            {
                ColumnIndex = googleSheetParameters.RangeColumnStart - 1,
                RowIndex = googleSheetParameters.RangeRowStart - 1,
                SheetId = sheetId
            };

            var request = new Request {UpdateCells = new UpdateCellsRequest {Start = gc, Fields = "*"}};

            var listRowData = new List<RowData>();

            foreach (var row in rows)
            {
                var rowData = new RowData();
                var listCellData = new List<CellData>();
                foreach (var cell in row.Cells)
                {
                    var cellData = new CellData();
                    var extendedValue = new ExtendedValue {StringValue = cell.CellValue};

                    cellData.UserEnteredValue = extendedValue;
                    var cellFormat = new CellFormat {TextFormat = new TextFormat()};

                    if (cell.IsBold)
                    {
                        cellFormat.TextFormat.Bold = true;
                    }

                    cellFormat.BackgroundColor = new Google.Apis.Sheets.v4.Data.Color { Blue = (float)cell.BackgroundColor.B/255, Red = (float)cell.BackgroundColor.R/255, Green = (float)cell.BackgroundColor.G/255 };

                    cellData.UserEnteredFormat = cellFormat;
                    listCellData.Add(cellData);
                }
                rowData.Values = listCellData;
                listRowData.Add(rowData);
            }
            request.UpdateCells.Rows = listRowData;

            // It's a batch request so you can create more than one request and send them all in one batch. Just use reqs.Requests.Add() to add additional requests for the same spreadsheet
            requests.Requests.Add(request);

            _sheetsService.Spreadsheets.BatchUpdate(requests, _spreadsheetId).Execute();
        }

        private string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];
            return value;
        }

        private GoogleSheetParameters MakeGoogleSheetDataRangeColumnsZeroBased(GoogleSheetParameters googleSheetParameters)
        {
            googleSheetParameters.RangeColumnStart = googleSheetParameters.RangeColumnStart - 1;
            googleSheetParameters.RangeColumnEnd = googleSheetParameters.RangeColumnEnd - 1;
            return googleSheetParameters;
        }


         */
    }

    public class GoogleSheetCell
    {
        public string CellValue { get; set; }
        public bool IsBold { get; set; }
        public System.Drawing.Color BackgroundColor { get; set; } = System.Drawing.Color.White;
    }

    public class GoogleSheetParameters
    {
        public int RangeColumnStart { get; set; }
        public int RangeRowStart { get; set; }
        public int RangeColumnEnd { get; set; }
        public int RangeRowEnd { get; set; }
        public string SheetName { get; set; }
        public bool FirstRowIsHeaders { get; set; }    
    }

    public class GoogleSheetRow
    {
        public GoogleSheetRow() => Cells = new List<GoogleSheetCell>();

        public List<GoogleSheetCell> Cells { get; set; }
    }
}