using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEditor;

namespace Daily.G2U
{
    public static class DailyG2UManager
    {
        public const string GoogleSheetConfigPath = "Assets/Plugins/G2U/Editor/GoogleSheetConfig.asset";
        public const string GoogleSheetCachePath = "Assets/Plugins/G2U/Editor/GoogleSheetCreateCache.asset";

        private static GoogleSheetConfig _config;
        public static GoogleSheetConfig Config
        {
            get
            {
                if (_config == null)
                {
                    _config = LoadOrCreateConfig();
                }
                return _config;
            }
        }

        [InitializeOnLoadMethod]
        public static void InitDailyG2U()
        {
            LoadOrCreateConfig();
        }

        private static GoogleSheetConfig LoadOrCreateConfig()
        {
            var getGuid = AssetDatabase.FindAssets($"t:{nameof(GoogleSheetConfig)}");
            if (getGuid.Length > 0)
                _config = AssetDatabase.LoadAssetAtPath<GoogleSheetConfig>(AssetDatabase.GUIDToAssetPath(getGuid[0]));

            if (_config == null)
            {
                var folderPath = "Assets/Plugins";

                if (!AssetDatabase.IsValidFolder(folderPath))
                    AssetDatabase.CreateFolder("Assets", "Plugins");
                
                if (!AssetDatabase.IsValidFolder(folderPath+"/G2U"))
                    AssetDatabase.CreateFolder(folderPath, "G2U");
                
                if (!AssetDatabase.IsValidFolder(folderPath+"/G2U/Editor"))
                    AssetDatabase.CreateFolder(folderPath+"/G2U", "Editor");
                
                
                _config = ScriptableObject.CreateInstance<GoogleSheetConfig>();
                AssetDatabase.CreateAsset(_config, DailyG2UManager.GoogleSheetConfigPath);
            }

            return _config;
        }

        public static async void GetData(string sheetId, string worksheetName, Action<G2UTable> call)
        {
            if (string.IsNullOrEmpty(sheetId) || string.IsNullOrEmpty(worksheetName))
            {
                Debug.LogWarning("表單ID為空!");
                return;
            }
            await GoogleAuth2Utility.CheckForRefreshOfToken();

            var sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + sheetId);
            sb.Append("/values");
            sb.Append("/" + worksheetName);
            sb.Append("?access_token=" + Config.response.access_token);

            using (UnityWebRequest request = UnityWebRequest.Get(sb.ToString()))
            {
                await request.SendWebRequest();
                if (request.downloadHandler.text.Contains("error"))
                {
                    Debug.LogError(request.downloadHandler.text);
                    if(request.downloadHandler.text.Contains("PERMISSION"))
                    {
                        GoogleSheetConfigEditor.ShowWindow();
                    }
                    return;
                }
                var s = JsonConvert.DeserializeObject<G2UTable>(request.downloadHandler.text);
                call?.Invoke(s);
            }
        }

        public static async void CreateNewWorkSheet(string sheetId, string sheetName, string past)
        {
            await GoogleAuth2Utility.CheckForRefreshOfToken();
            var newGoogle = new GoogleSheetsHelper(Config.response.access_token, sheetId);
            newGoogle.AddWorkSheet(sheetId, sheetName, past);
        }
    }
    public static class ExtensionMethods
    {
        public static System.Runtime.CompilerServices.TaskAwaiter GetAwaiter(this AsyncOperation asyncOp)
        {
            var tcs = new TaskCompletionSource<object>();
            asyncOp.completed += _ => { tcs.SetResult(null); };
            return ((Task)tcs.Task).GetAwaiter();
        }
    }
}