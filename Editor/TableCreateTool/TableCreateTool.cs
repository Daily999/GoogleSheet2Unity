﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

using UnityEngine.UIElements;
namespace Daily.G2U.TableCreateTool
{
    public class TableCreateTool : EditorWindow
    {
        private List<string> typeList = new List<string>();
        private List<string> pick = new List<string>();
        private string t1;
        private string t2;
        private string _workSheetName;

        
        [MenuItem("Tools/GoogleSheetToUnity/表單創建")]
        private static void ShowWindow()
        {
            var window = GetWindow<TableCreateTool>();
            window.titleContent = new GUIContent("TableCreateTool");
            window.Show();
        }

        public void CreateGUI()
        {
            var title = new Label();
            title.text = "列表建立工具";
            
            var classNameInput = new TextField();
            classNameInput.label = "Class Name";
            classNameInput.value = _workSheetName;
            
            var classDropdown = new DropdownField();
            classDropdown.choices = pick;
            
            var sheetId = new TextField();
            sheetId.label = "Google Sheet ID";
            sheetId.value = t1;
            
            var newWorkSheetName = new TextField();
            newWorkSheetName.label = "新工作表名稱";
   
            var createTableBt = new Button(() => { CreateTable(classDropdown.value, _workSheetName); });
            createTableBt.text = "建立Sheet";
            
            classNameInput.RegisterCallback<InputEvent>(evt =>
            {
                _workSheetName = evt.newData;
            });
            classNameInput.RegisterCallback<InputEvent>(evt =>
            {
                typeList.Clear();
                pick.Clear();
                
                var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (var item in allAssemblies)
                {
                    var ts = item.GetTypes();
                    foreach (var t2 in ts)
                    {
                        if (t2.FullName.ToUpper().Contains(evt.newData.ToUpper()))
                        {
                            pick.Add(t2.FullName);
                        }
                    }
                    if (pick.Count >= 20)
                    {
                        pick.Add("匹配的類別過多，已隱藏部分內容");
                        break;
                    }
                }
                
                if (pick.Count == 0)
                {
                    pick.Add("沒有找到的類別");
                }
                
                classDropdown.choices = pick;
                classDropdown.index = 0;
            });
            sheetId.RegisterCallback<InputEvent>(evt =>
            {
                t1 = evt.newData;
            });       
            newWorkSheetName.RegisterCallback<InputEvent>(evt =>
            {
                t2 = evt.newData;
            });
            
            rootVisualElement.Add(title);   
            rootVisualElement.Add(sheetId);   
            rootVisualElement.Add(newWorkSheetName);   
            rootVisualElement.Add(classNameInput);   
            rootVisualElement.Add(classDropdown);
            rootVisualElement.Add(createTableBt);   
        }
        
        private void CreateTable(string targetName, string nameTxt)
        {
            Type targetType = null;
            var allAssemblies=  AppDomain.CurrentDomain.GetAssemblies();
            foreach (var item in allAssemblies)
            {
                targetType = item.GetType(targetName);
                if (targetType != null)
                    break;
            }
            
            if (targetType == null)
            {
                Debug.LogWarning($"無法建立該類別! {nameTxt}");
                return;
            }
            
            var data = new ScriptTableData();
            data.ClassName = nameTxt;
            
            var fields = targetType.GetFields( BindingFlags.Public | BindingFlags.NonPublic| BindingFlags.Instance );
            foreach (var field in fields)
            {
                data.FiledList.Add(new FiledData
                {
                    FiledName = field.Name,
                    Type = GetType(field.FieldType),
                    IsArray = field.GetType().IsArray
                });
            }
            DailyG2UManager.CreateNewWorkSheet(t1, data.ClassName,data.GetTableTxt());
        }

//        private void SelectScript()
//        {
//            var path= EditorUtility.OpenFilePanel("選擇腳本", Application.dataPath, "cs");
//           path = "Assets" + path.Substring(Application.dataPath.Length);
//           var script = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
//           if(script != null)
//           {
//               if (script.text.Contains("Behaviour"))
//               {
//                   Debug.LogWarning("選擇的腳本不能是Mono腳本");
//                   //檢查單腳本的類別數量
//                   return;
//               }
//               ParseScript(script.text, script.name);
//           }
//        }
//
//        private void ParseScript(string text, string nameTxt)
//        {
//            var data = new ScriptTableData();
//            data.ClassName = nameTxt;
//
//            lines = text.Replace("\n\r", "\n").Replace("\r\n", "\n").Split('\n');
//            var namespaceName= FindNameSpace();
//
//            Type targetType=null;
//            var allAssemblies=  AppDomain.CurrentDomain.GetAssemblies();
//            foreach (var item in allAssemblies)
//            {
//                targetType = item.GetType($"{namespaceName}.{nameTxt}");
//                if (targetType != null)
//                {
//                    break;
//                }
//            }
//
//            if (targetType == null)
//            {
//                Debug.LogWarning($"找不到類別 {nameTxt}");
//                return;
//            }
//
//            var fields = targetType.GetFields( BindingFlags.Public | BindingFlags.NonPublic| BindingFlags.Instance );
//            foreach (var field in fields)
//            {
//                data.FiledList.Add(new FiledData
//                {
//                    FiledName = field.Name,
//                    Type = GetType(field.FieldType),
//                    IsArray = field.GetType().IsArray
//                });
//            }
//            DailyG2UManager.CreateNewWorkSheet(t1,data.ClassName,data.GetTableTxt());
//            Debug.Log(data.GetTableTxt());
//        }
        
        private FiledType GetType(Type t)
        {
            if (t == typeof(string)) return FiledType.String;
            if (t == typeof(int)) return FiledType.Int;
            if (t == typeof(float)) return FiledType.Float;
            if (t == typeof(bool)) return FiledType.Bool;
            return FiledType.Unknown;
        }

//        private string FindNameSpace()
//        {
//            for (var index = 1; index < lines.Length; index++)
//            {
//                var line = GetLine(index);
//                if (!line.Contains(@"//") && line.Contains("namespace"))
//                {
//                    var texts = line.Split(" ");
//                    return texts[1];
//                }
//            }
//            return null;
//        }
//
//        private string[] lines;
//        private string GetLine(int lineNo)
//        {
//            return lines.Length >= lineNo ? lines[lineNo-1] : null;
//        }
    }
}