using UnityEngine;
using System.Collections.Generic;
using Daily.G2U;
using UnityEditor;

public class G2UCache : ScriptableObject
{
    public static G2UCache cache;
    public string tmpSpreadSheetID;
    public List<CreateTmpData> tmpData = new List<CreateTmpData>();
    public string savePathCache;

    public CreateTmpData TryGetCache(string name)
    {
       return tmpData.Find(x => x.className == name);
    }
    
    public static G2UCache LoadOrCreateCache()
    {
        var getGUID = UnityEditor.AssetDatabase.FindAssets("t:G2UCache");
        if (getGUID.Length > 0)
            cache = AssetDatabase.LoadAssetAtPath<G2UCache>(AssetDatabase.GUIDToAssetPath(getGUID[0]));

        if (cache == null)
        {
            cache = ScriptableObject.CreateInstance<G2UCache>();
            if (!AssetDatabase.IsValidFolder("Assets/Editor"))
                AssetDatabase.CreateFolder("Assets", "Editor");

            AssetDatabase.CreateAsset(cache, DailyG2UManager.GoogleSheetCachePath);
        }
        return cache;
    }
}

[System.Serializable]
public class CreateTmpData
{
    public string workSheetName;
    public string className;
    public List<ClassCreatorItem> fields = new List<ClassCreatorItem>();

    public CreateTmpData(string workSheetName, string className, List<ClassCreatorItem> fields)
    {
        this.workSheetName = workSheetName;
        this.className = className;
        this.fields.AddRange(fields); 
    }
}