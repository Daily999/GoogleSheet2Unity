using UnityEditor;
using UnityEngine;

namespace Daily.G2U
{
    public class GoogleSheetConfig : ScriptableObject
    {
        public string CLIENT_ID = "";
        public string CLIENT_SECRET = "";
        public string ACCESS_TOKEN = "";

        [HideInInspector]
        public string REFRESH_TOKEN;
        public string API_KEY = "";
        public int PORT = 80;
        public GoogleDataResponse response;

        public void Save()
        {
            EditorPrefs.SetString("G2U_CLIENT_ID", CLIENT_ID);
            EditorPrefs.SetString("G2U_CLIENT_SECRET", CLIENT_SECRET);
            EditorPrefs.SetString("G2U_ACCESS_TOKEN", ACCESS_TOKEN);
            EditorPrefs.SetString("G2U_REFRESH_TOKEN", REFRESH_TOKEN);
            EditorPrefs.SetString("G2U_API_KEY", API_KEY);
            EditorPrefs.SetInt("G2U_PORT", PORT);
            response.Save();
        }
        
        public void Load()
        {
            CLIENT_ID = EditorPrefs.GetString("G2U_CLIENT_ID", CLIENT_ID);
            CLIENT_SECRET = EditorPrefs.GetString("G2U_CLIENT_SECRET", CLIENT_SECRET);
            ACCESS_TOKEN = EditorPrefs.GetString("G2U_ACCESS_TOKEN", ACCESS_TOKEN);
            REFRESH_TOKEN = EditorPrefs.GetString("G2U_REFRESH_TOKEN", REFRESH_TOKEN);
            API_KEY = EditorPrefs.GetString("G2U_API_KEY", API_KEY);
            PORT = EditorPrefs.GetInt("G2U_PORT", PORT);
            response = new GoogleDataResponse().Load();
        }

        [System.Serializable]
        public class GoogleDataResponse
        {
            public string access_token = "";
            public string refresh_token = "";
            public string token_type = "";
            public int expires_in = 0; //just a place holder to work the the json and caculate the next refresh time
            public System.DateTime nextRefreshTime;
            public void Save()
            {
                EditorPrefs.SetString("G2U_ACCESS_TOKEN", access_token);
                EditorPrefs.SetString("G2U_REFRESH_TOKEN", refresh_token);
                EditorPrefs.SetString("G2U_TOKEN_TYPE", token_type);
                EditorPrefs.SetInt("G2U_EXPIRES_IN", expires_in);
            }
            public GoogleDataResponse Load()
            {
                access_token = EditorPrefs.GetString("G2U_ACCESS_TOKEN", access_token);
                refresh_token = EditorPrefs.GetString("G2U_REFRESH_TOKEN", refresh_token);
                token_type = EditorPrefs.GetString("G2U_TOKEN_TYPE", token_type);
                expires_in = EditorPrefs.GetInt("G2U_EXPIRES_IN", expires_in);
                return this;
            }
        }
    }
}