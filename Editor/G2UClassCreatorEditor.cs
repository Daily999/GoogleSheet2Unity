using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine.UIElements;

namespace Daily.G2U
{
    public class G2UClassCreatorEditor : EditorWindow
    {
        public string spreadSheetID;
        public string workSheetName;
        public string className;

        private G2UCache _cache;
        public G2UCache Cache
        {
            get
            {
                if (_cache == null)
                    _cache = G2UCache.LoadOrCreateCache();
                return _cache;
            }
        }
        CellType s;
        public List<ClassCreatorItem> fields = new List<ClassCreatorItem>();

        [MenuItem("Tools/GoogleSheetToUnity/SO類別創建")]
        private static void ShowWindow()
        {
            var window = GetWindow<G2UClassCreatorEditor>();
            window.titleContent = new GUIContent("ScriptableObjectCreator");
            window.Show();

            var cache = G2UCache.LoadOrCreateCache();
            window.spreadSheetID = cache.tmpSpreadSheetID;
            
        }


        private VisualElement fieldsShow;

        public void CreateGUI()
        {
            var title = new Label();
            title.text = "類別創建工具";

            var sheetId = new TextField();
            sheetId.label = "Google Sheet ID";
            spreadSheetID = Cache.tmpSpreadSheetID;
            sheetId.value = spreadSheetID;

            var newWorkSheetName = new TextField();
            newWorkSheetName.label = "工作表名稱";
            newWorkSheetName.value = workSheetName;

            var classNameInput = new TextField();
            classNameInput.label = "新類別名稱";
            classNameInput.value = className;

            var getSheetData = new Button(GetSheetData);
            getSheetData.text = "取得表單資料";
//            
            fieldsShow = new VisualElement();
            fieldsShow.style.marginLeft = new StyleLength(new Length(8, LengthUnit.Pixel));
            fieldsShow.style.marginBottom = new StyleLength(new Length(8, LengthUnit.Pixel));
            fieldsShow.style.marginRight = new StyleLength(new Length(8, LengthUnit.Pixel));
            fieldsShow.style.marginTop = new StyleLength(new Length(8, LengthUnit.Pixel));

            var createClass = new Button(CreateClass);
            createClass.text = "創建類別";


            sheetId.RegisterCallback<InputEvent>(evt => { spreadSheetID = evt.newData; });
            newWorkSheetName.RegisterCallback<InputEvent>(evt => { workSheetName = evt.newData; });
            classNameInput.RegisterCallback<InputEvent>(evt => { className = evt.newData; });

            rootVisualElement.Add(title);
            rootVisualElement.Add(sheetId);
            rootVisualElement.Add(newWorkSheetName);
            rootVisualElement.Add(classNameInput);
            rootVisualElement.Add(getSheetData);
            rootVisualElement.Add(fieldsShow);
            rootVisualElement.Add(createClass);
            CreateFields(fields);
        }

        private void CreateFields(List<ClassCreatorItem> list)
        {
            fieldsShow.Clear();
            foreach (var item in list)
            {
                var newItem = new VisualElement();
                var name = new Label(item.fieldName);
                var field = new EnumField("", CellType.Undefined);
                field.value = item.type;

                var isArray = new Toggle("陣列")
                {
                    value = item.isArray
                };
                isArray.value = item.isArray;

                isArray.RegisterCallback<ChangeEvent<bool>>(evt =>
                {
                    item.isArray = evt.newValue;
                });
                field.RegisterCallback<ChangeEvent<Enum>>(evt =>
                {
                    item.type = (CellType)evt.newValue;
                });

                name.style.width = new StyleLength(new Length(120, LengthUnit.Pixel));
                field.style.width = new StyleLength(new Length(200, LengthUnit.Pixel));
                newItem.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
                newItem.style.marginBottom = new StyleLength(new Length(8, LengthUnit.Pixel));

                newItem.Add(name);
                newItem.Add(field);
                newItem.Add(isArray);
                fieldsShow.Add(newItem);
            }

        }


        private void GetSheetData()
        {
            fields.Clear();
            if (!string.IsNullOrEmpty(className))
            {
                var cache = Cache.TryGetCache(className);
                if (cache != null)
                {
                    fields.AddRange(cache.fields);
                }
            }

            if (!string.IsNullOrEmpty(spreadSheetID) && !string.IsNullOrEmpty(workSheetName))
                DailyG2UManager.GetData(spreadSheetID, workSheetName, OnDataLoaded);
        }

        private void CreateClass()
        {
            if (string.IsNullOrEmpty(className))
            {
                Debug.LogError("類別名稱不可為空");
                return;
            }

            CreateDataGroup();
            CreateDataClass();
            SaveCache();

            Debug.Log("已生成類別");
        }
        private void CreateDataGroup()
        {
            var p = AssetDatabase.FindAssets("ScriptableObjectClass");
            var s = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(p[0])).text;
            var tmpPath = Cache.savePathCache;

            var path = EditorUtility.SaveFilePanel("選擇要保存的位子", string.IsNullOrEmpty(tmpPath) ? Application.dataPath : tmpPath, $"{className}Group", "cs");
            Cache.savePathCache = path;

            using (var writer = new StreamWriter(path))
            {
                s = s.Replace("$SpreadSheetName", spreadSheetID);
                s = s.Replace("$WorksheetName", workSheetName);
                s = s.Replace("$ClassName", className + "Group");
                s = s.Replace("$DataClassName", className);

                writer.Write(s);
                writer.Close();
            }

            AssetDatabase.Refresh();
        }

        private void CreateDataClass()
        {
            var p = AssetDatabase.FindAssets("DataClass");
            var s = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(p[0])).text;
            var tmpPath = Cache.savePathCache;
            var path = EditorUtility.SaveFilePanel("選擇要保存的位子", string.IsNullOrEmpty(tmpPath) ? Application.dataPath : tmpPath, $"{className}", "cs");
            Cache.savePathCache = path;

            using (var writer = new StreamWriter(path))
            {
                s = s.Replace("$ClassName", className);

                var sb = new StringBuilder();
                foreach (var item in fields)
                {
                    var type = item.isArray ? item.Type + "[]" : item.Type;
                    var name = item.fieldName.ToLower();
                    var ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;

                    var getSetMethod = " { get => $Field; set => $Field = value; }\n\n";
                    getSetMethod = getSetMethod.Replace("$Field", name);

                    sb.Append($"\t[SerializeField]\n");
                    sb.Append($"\tprivate {type} {name};\n\n");
                    sb.Append($"\tpublic {type} {ti.ToTitleCase(name)}");
                    sb.Append(getSetMethod);
                }
                s = s.Replace("$MemberFields", sb.ToString());

                writer.Write(s);
                writer.Close();
            }

            AssetDatabase.Refresh();
        }
        private void OnDataLoaded(G2UTable obj)
        {
            foreach (var item in obj.values[0])
            {
                if (string.IsNullOrEmpty(item) || item.Contains("//"))
                    continue;

                if (fields.Find(x => x.fieldName == item) == null)
                    fields.Add(new ClassCreatorItem(item));
            }


            CreateFields(fields);
        }

        private void SaveCache()
        {
            var i = Cache.tmpData.FindIndex(x => x.className == className);
            if (i != -1)
            {
                Cache.tmpData.RemoveAt(i);
            }
            Cache.tmpData.Add(new CreateTmpData(workSheetName, className, fields));

            EditorUtility.SetDirty(Cache);
            AssetDatabase.SaveAssets();
        }
    }

    [System.Serializable]
    public class ClassCreatorItem
    {
        public string fieldName;
        public bool isArray = false;
        public CellType type = default;
        public string Type
        {
            get
            {
                switch (type)
                {
                    case CellType.String:
                        return "string";
                    case CellType.Float:
                        return "float";
                    case CellType.Int:
                        return "int";
                    case CellType.Bool:
                        return "bool";
                }
                return "string";
            }
        }

        public ClassCreatorItem(string name)
        {
            fieldName = name;
        }
    }

    public enum CellType
    {
        Undefined,
        String,
        Float,
        Int,
        Bool,
    }
}