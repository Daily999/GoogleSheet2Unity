using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Daily.G2U
{
    public static class TableUpdateHelper
    {
        public static void SetTable(G2UTable txt, Type[] targetType, Object target)
        {
            var listType = typeof(List<>).MakeGenericType(targetType);
            var newDataTable = Activator.CreateInstance(listType) as IList;
            if (newDataTable == null)
                return;
            var missingField = new List<string>();
            var format = GetFormatList(txt.values[0]);
            
            for (int x = 1; x < txt.values.Count; x++)
            {
                var rows = txt.values[x];
                if (rows == null || rows.Count == 0 || rows[0].Contains("//"))
                {
                    // 此列為空跳過 此列開頭為//跳過整行
                    continue;
                }

                var newObject = CreateObject(targetType);
                var type = newObject.GetType();
                
                for (int y = 0; y < rows.Count; y++)
                {
                    // 檢查格式 格式有// 或是空 或是 超出格式跳過
                    if  ((format.Count - 1 < y) ||
                        format[y].name.Contains("//") ||
                        txt.values[0][y].Contains("Unknown"))
                        continue;

                    var element = txt.values[x][y];
                    var field = type.GetField(format[y].name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    if (field == null)
                    {
                        if (!missingField.Contains(format[y].name))
                            missingField.Add(format[y].name);
                        continue;
                    }
                    try
                    {
                        SetObjectValue(field, newObject, element);
                    }
                    catch (FormatException)
                    {
                        Debug.LogError($"{format[y]} 第 {x} 元素 {element} 資料格式無法辨識，請確認輸入格式是否正確。");
                    }
                }

                newDataTable.Add(newObject);
            }
            
            if (missingField.Count > 0)
                Debug.LogWarning($"{string.Join(",", missingField)} 欄位不存在，請確認輸入格式是否正確。");
            
            if (targetType[0].IsSubclassOf(typeof(ScriptableObject)))
                SaveScriptableObjectAssets(newDataTable, target);

            var newTargetType = typeof(IG2UTable<>).MakeGenericType(targetType);
            var setListMethod = newTargetType.GetMethod("UpdateTable");
            if (setListMethod != null)
                setListMethod.Invoke(target, new object[] { newDataTable });
        }

        private static void SaveScriptableObjectAssets(IList list, Object obj)
        {
            MethodInfo setNameMethod = null;
            var tableType = obj.GetType().GetInterface("IG2USubAssetName`1");

            if (tableType == null)
            {
                Debug.LogWarning($"{obj.name} 未實作IG2USubAssetName介面，無法自動產生ScriptableObject物件。");
                return;
            }
            
            var objs = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(obj));
            var tableDataType = tableType.GetGenericArguments();
            var newTargetType = typeof(IG2USubAssetName<>).MakeGenericType(tableDataType);
            setNameMethod = newTargetType.GetMethod("GetSubAssetName");


            for (var index = 0; index < list.Count; index++)
            {
                var item = list[index];
                if (item is ScriptableObject so)
                {
                    if (setNameMethod != null)
                    {
                        so.name = (string)setNameMethod.Invoke(obj, new object[] { item });
                        var isExist = false;
                        foreach (var item2 in objs)
                        {
                            var s = item2 as ScriptableObject;
                            if (s.name == so.name)
                            {
                                var json = EditorJsonUtility.ToJson(so);
                                EditorJsonUtility.FromJsonOverwrite(json, s);
                                isExist = true;
                                list[index]=s;
                                break;
                            }
                        }
                        if (isExist == false)
                            AssetDatabase.AddObjectToAsset(so, obj);
                    }
                }
            }


            AssetDatabase.SetMainObject(obj, AssetDatabase.GetAssetPath(obj));
        }

        private static List<(string type, string name)> GetFormatList(List<string> row)
        {
            var format = new List<(string, string)>();
            var formatList = row;
            for (int i = 0; i < formatList.Count; i++)
            {
                if (formatList[i].Contains("//"))
                {
                    format.Add(("string", "//"));
                    continue;
                }


                var formatSplit = formatList[i].Split(" ");
                if (formatSplit.Length < 2)
                {
                    Debug.LogWarning($"{formatList[i]} 格式錯誤，請確認格式是否正確。");
                    continue;
                }
                
                format.Add((formatSplit[0], formatSplit[1]));
            }
            return format;
        }

        private static void SetObjectValue(FieldInfo field, object data, string element)
        {
            if (field.FieldType.IsEnum)
            {
                try
                {
                    var type = field.FieldType;
                    var test= Enum.Parse(type,element);
                    field.SetValue(data,test);
                }
                catch (ArgumentException e)
                {
                    Debug.LogError($"{element} 無法解析");
                }
            
                return;
            }

            if (field.FieldType.IsValueType)
            {
                if (field.FieldType == typeof(string))
                {
                    field.SetValue(data, element);
                    return;
                }

                if (bool.TryParse(element, out var isBool))
                {
                    field.SetValue(data, isBool);
                    return;
                }

                if (int.TryParse(element, out var f))
                {
                    field.SetValue(data, f);
                    return;
                }

                field.SetValue(data, float.Parse(element));
                return;
            }

            if (field.FieldType.IsArray)
            {
                var txtArray = element.Split(',');

                var stringArray = new List<string>();
                stringArray.AddRange(txtArray);

                if (field.FieldType == typeof(int[]))
                {
                    field.SetValue(data, stringArray.ConvertAll(O2I).ToArray());
                    return;
                }

                if (field.FieldType == typeof(float[]))
                {
                    field.SetValue(data, stringArray.ConvertAll(O2F).ToArray());
                    return;
                }
                
                field.SetValue(data, stringArray.ToArray());
                return;
            }

            field.SetValue(data, element);
        }

        private static object CreateObject(Type[] targetType)
        {
            var type = targetType[0];
            object newObj;

            if (type.IsSubclassOf(typeof(ScriptableObject)))
            {
                newObj = ScriptableObject.CreateInstance(type);
            }
            else
            {
                newObj = Activator.CreateInstance(type);
            }

            return newObj;
        }

        private static int O2I(string input)
        {
            return int.Parse(input);
        }
        private static float O2F(string input)
        {
            return float.Parse(input);
        }

        public static List<ScriptableObject> GetAllTable()
        {
            var tableList = new List<ScriptableObject>();
            var allSo = AssetDatabase.FindAssets($"t:{nameof(ScriptableObject)}");
            if (allSo.Length > 0)
            {
                for (int i = 0; i < allSo.Length; i++)
                {
                    var table = AssetDatabase.LoadAssetAtPath<ScriptableObject>(AssetDatabase.GUIDToAssetPath(allSo[i]));
                    if (table.GetType().GetInterface("IG2UTable`1") != null)
                    {
                        tableList.Add(table);
                    }
                }
            }
            return tableList;
        }

    }
}